import { Fighter } from "./model/Fighter";
import View from './view';
import { Atribute } from "./model/Attribute";
import fightersInfo from './Data/data'

export class DialogWindow extends View{
    public element: HTMLDialogElement;
    fighter: Fighter;
    constructor(fighter: Fighter){
        super();
        this.fighter = fighter;

        this.addDataFields();
        
        const button = this.createElement({
            tagName: 'button',
            className: 'dialogButton'
          });
          button.innerHTML = "Save and close";
        button.addEventListener('click', () => {
            let attackEl = document.getElementById('attackIB') as HTMLInputElement;
            this.fighter.attack = Number.parseInt(attackEl.value);
            let defenceEl = document.getElementById('defenceIB') as HTMLInputElement;
            this.fighter.defense = Number.parseInt(defenceEl.value);
            let healthEl = document.getElementById('healthIB') as HTMLInputElement;
            this.fighter.health = this.fighter.maxHealth = Number.parseInt(healthEl.value);
            fightersInfo.fightersInfo.set(this.fighter._id, this.fighter);
            this.hide();
        });
        this.element.appendChild(button);
    }

    addDataFields(){
        const headerElement = this.createElement({
            tagName: 'h1',
            className: 'header'
          });
        headerElement.innerHTML = this.fighter._id + " " + this.fighter.name;
        

        this.element = document.createElement('dialog');
        this.element.classList.add("dialog");
        this.element.appendChild(headerElement);
        this.element.appendChild(this.generateTextBox('Attack'));
        this.element.appendChild(this.generateInputBox('attackIB', this.fighter.attack == undefined ? '0' : this.fighter.attack.toString()));
        this.element.appendChild(this.generateTextBox('Defence'));
        this.element.appendChild(this.generateInputBox('defenceIB', this.fighter.defense == undefined ? '0' : this.fighter.defense.toString()));
        this.element.appendChild(this.generateTextBox('Health'));
        this.element.appendChild(this.generateInputBox('healthIB', this.fighter.health == undefined ? '0' : this.fighter.health.toString()));
    }

    generateInputBox(Id: string, value: string)  : HTMLElement{
        const attributes = [
            new Atribute("type", "text"),
            new Atribute("value", value)
        ];

        const inputElement = this.createElement({
            tagName: 'input',
            className: 'fighterCharacteristics',
            attributes
          });
          inputElement.id = Id;
          return inputElement;
    }
    generateTextBox(text: string) : HTMLElement {
        const labelElement = this.createElement({
            tagName: 'h3',
            className: 'dialogH3'
          });
          labelElement.innerHTML = text;
          return labelElement;
    }

    show(): void {
        this.element.show();
    }

    hide(): void {
        this.element.close();
    }
}
