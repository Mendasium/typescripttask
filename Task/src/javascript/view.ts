import { Atribute } from './model/Attribute';

class View {
  element: HTMLElement;
//Чем тут является атрибут
  createElement({ tagName, className = '', attributes = [] } : { tagName : string, className?: string, attributes? : Atribute[]  }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    attributes.forEach(atr => element.setAttribute(atr.key, atr.value));

    return element;
  }
}

export default View;
