import View from './view';
import FighterView from './fighterView';
import { Fighter } from './model/Fighter';
import { DialogWindow } from './dialogWindow';
import { fighterService } from './services/fightersService';
import fightersInfo from './Data/data';

class FightersView extends View {
  handleClick: Function;

  constructor(fighters: Fighter[]) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map<number, Fighter>();

  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event: Event, fighterId: number) {
    let fighter: Fighter;
    if(!fightersInfo.fightersInfo.has(fighterId)){
      fighter = await fighterService.getFighterDetails(fighterId);
      fighter.maxHealth = fighter.health;
      fightersInfo.fightersInfo.set(fighterId, fighter);
    } else{
      fighter = fightersInfo.fightersInfo.get(fighterId);
    }
    const dlg = document.getElementById("dialogPlace");
    dlg.innerHTML="";
    const dialog = new DialogWindow(fighter);
    dlg.appendChild(dialog.element);
    dialog.show();
  }
}

export default FightersView;