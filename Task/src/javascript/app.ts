import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from './model/Fighter';
import { FighterInterface } from './fighterInterface';
import fightersInfo from './Data/data';

class App {
  constructor() {
    this.startApp();
  }
  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      const startButton = document.createElement('button');
      startButton.className = "startFightButton";
      startButton.id = "startFightButton";
      startButton.innerHTML = "Start Game";
      startButton.addEventListener('click', (event: Event) => this.startGame(), false);;

      App.rootElement.appendChild(fightersElement);
      App.rootElement.appendChild(startButton);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  startGame() {
    const checkboxes =  Array.from(document.getElementsByClassName("fighter-checkbox"));
    let checkedFighters : Fighter[] = new Array();
    let fighters = fightersInfo.fightersInfo;
    checkboxes.forEach((el : HTMLInputElement) => {
      if(el.checked){
        let fighterId = Number.parseInt(el.classList[1].substr(el.classList[1].lastIndexOf('-') + 1));

        fighters.forEach(element => {
          //console.log(typeof(element._id)); //Почему оно string?
          if(element._id == fighterId)
            checkedFighters.push(element);
        });
      }
    });
    if(checkedFighters.length === 2)
    {
      const button = document.getElementById("startFightButton");
      button.classList.remove("error");
      button.innerHTML = "Start Game"
      let fighterInterface = new FighterInterface(checkedFighters.pop(), checkedFighters.pop());
      const dlg = document.getElementById("dialogPlace");
      dlg.innerHTML = "";
      dlg.appendChild(fighterInterface.element);
      fighterInterface.show();
    }
    else{
      const button = document.getElementById("startFightButton");
      button.classList.add("error");
      button.innerHTML = "Choose two fighters for battle, then click here!"
      console.log("Choose two fighters!");
    }
  }
}

export default App;