import { Fighter } from './model/Fighter';
import View from './view';
import { Atribute } from './model/Attribute';

export class FighterInterface extends View {
    public element: HTMLDialogElement;
    fighterOne: Fighter;
    fighterTwo: Fighter;
    isStopped: boolean = true;
    fighterOneTurn: boolean = true;


    constructor(fOne: Fighter, fTwo: Fighter) {
        super();
        this.fighterOne = fOne;
        this.fighterTwo = fTwo;
        this.fighterOne.health = this.fighterOne.maxHealth;
        this.fighterTwo.health = this.fighterTwo.maxHealth;

        this.element = document.createElement('dialog');
        this.element.classList.add("dialogFight");
        const leftFighter = this.generateDiv("leftFighter");
        const fight = this.generateDiv("fightBlock");
        fight.id = "fightBlock";
        const rightFighter = this.generateDiv("rightFighter");

        const leftFighterHP = this.generateDiv("leftFighterHP");
        leftFighterHP.id = "leftFighterHP";
        const rightFighterHP = this.generateDiv("rightFighterHP");
        rightFighterHP.id = "rightFighterHP";

        this.generateFieldsForFighter(this.fighterOne, leftFighter);
        this.generateFieldsForFighter(this.fighterTwo, rightFighter);


        this.element.append(leftFighter, leftFighterHP, fight, rightFighterHP, rightFighter);
        this.createButtons();
    }

    createButtons() {
        const buttonStartStop = this.createElement({
            tagName: 'button',
            className: 'startButton'
        });
        buttonStartStop.innerHTML = "Start"
        buttonStartStop.addEventListener('click', () => {
            this.isStopped = !this.isStopped;
            document.getElementsByClassName("startButton")[0].innerHTML = this.isStopped ? "Continue" : "Stop";
            if(!this.isStopped)
                this.StartFight();
        });

        const buttonClose = this.createElement({
            tagName: 'button',
            className: 'closeButton'
        });
        buttonClose.innerHTML = "Close";
        buttonClose.addEventListener('click', () => {
            this.isStopped = true;
            this.hide(); //Оно не работает
            document.getElementById("dialogPlace").innerHTML = "";
        });
        this.element.append(buttonStartStop, buttonClose);
    }

    generateFieldsForFighter(fighter: Fighter, element: HTMLElement){
        const headerElement = this.createElement({
            tagName: 'h1',
            className: 'header'
          });
        headerElement.innerHTML = fighter._id + " " + fighter.name;
        
        element.appendChild(headerElement);
        element.appendChild(this.generateImage(fighter.source));
        element.appendChild(this.generateTextBox('Attack: ' + fighter.attack));
        element.appendChild(this.generateTextBox('Defense: ' + fighter.defense));
        element.appendChild(this.generateTextBox('MaxHealth: ' + fighter.maxHealth));
    }

    private StartFight(){
        if(!this.isStopped)
        {
            let f1 = this.fighterOne;
            let f2 = this.fighterTwo;
            if(f1.health > 0 && f2.health > 0) {
                if(this.fighterOneTurn){
                    let damageOne = this.damageOpponent(f1, f2);
                    this.fighterTwo.health -= this.fighterTwo.health > damageOne ? damageOne : this.fighterTwo.health;
                    document.getElementById("rightFighterHP").style.top = (100 - (100 / this.fighterTwo.maxHealth * this.fighterTwo.health)) + "%";
                }
                else {
                    let damageTwo = this.damageOpponent(f2, f1);
                    this.fighterOne.health -= this.fighterOne.health > damageTwo ? damageTwo : this.fighterOne.health;;
                    document.getElementById("leftFighterHP").style.top = (100 - (100 / this.fighterOne.maxHealth * this.fighterOne.health)) + "%";    
                }
                setTimeout(() => this.StartFight(), 500);
            }
            else {
                this.isStopped = true;
                const startButton = document.getElementsByClassName('startButton')[0];
                startButton.parentNode.removeChild(startButton);

                if(f1.health === 0){
                    this.setWinner(f2.name, "leftFighter");
                }
                else{
                    this.setWinner(f1.name, "rightFighter");
                }
            }
        }
    }

    private setWinner(winnerName: string, looserClassName: string) : void{
        const innerHTML = document.getElementById("fightBlock").innerHTML;
        let winner;
        winner = "<p class=\"victory\">" + winnerName + " wins!!!</p>";
        document.getElementsByClassName(looserClassName)[0].classList.add("looser");
        document.getElementById("fightBlock").innerHTML = winner + innerHTML + winner;
    }
    private damageOpponent(attacker: Fighter, defender: Fighter) : number{
        let damage = this.getHitPoint(attacker.attack) - this.getBlockPower(defender.defense);
        damage = damage > 0 ? damage : 0;
        document.getElementById("fightBlock").innerHTML += "<p>" + attacker.name + " deal " + damage + " damage to " + defender.name + "</p>";
        return damage;
    }

    private getHitPoint(attack: number): number {
        let criticalHitChance = this._generateRandomNumber(2);
        let power = attack * criticalHitChance;

        return power;
    }
    
    private getBlockPower(defense: number): number {
        let criticalHitChance = this._generateRandomNumber(2);
        let power = defense * criticalHitChance;

        return power;
    }

    private _generateRandomNumber(maxValue: number) : number{
        return Math.round(Math.random() * maxValue) + 1;
    }


    generateImage(value: string) : HTMLElement{
        const attributes = [
            new Atribute("src", value)
        ];

        const image = this.createElement({
            tagName: 'img',
            className: 'fighterFimg',
            attributes
          });
          return image;
    }
    generateTextBox(text: string) : HTMLElement {
        const labelElement = this.createElement({
            tagName: 'h3',
            className: 'dialogH3'
          });
          labelElement.innerHTML = text;
          return labelElement;
    }
    generateDiv(elClass: string) : HTMLElement{
        const labelElement = this.createElement({
            tagName: 'div',
            className: elClass
          });
          return labelElement;
    }
    show(): void {
        this.element.show();
    }

    hide(): void {
        this.element.close();
    }
}