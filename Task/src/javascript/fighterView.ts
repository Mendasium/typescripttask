import View from './view';
import { Fighter } from './model/Fighter';
import { Atribute } from './model/Attribute';

class FighterView extends View {
  
  constructor(fighter: Fighter, handleClick: Function) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter: Fighter, handleClick: Function) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkBoxElement = this.createCheckBox();
    checkBoxElement.classList.add("CBFighter-" + fighter._id);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement, checkBoxElement);
    this.element.addEventListener('click', (event: Event) => handleClick(event, fighter._id), false);
  }

  createName(name: string) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source: string) {
    let attributes: Atribute[] = [
      new Atribute("src", source)
    ];
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }


  createCheckBox() {
    let attributes: Atribute[] = [
      new Atribute("type", "checkbox")
    ];
    const checkBoxElement = this.createElement({
      tagName: 'input',
      className: 'fighter-checkbox',
      attributes
    });
    return checkBoxElement;
  }
}


export default FighterView;