export interface IFighter {
    _id? : number;
    health : number;
    attack : number;
    defense : number;
    name : string;
    source: string;
    getHitPoint() : number;
    getBlockPower() : number;
}

export class Fighter implements IFighter {
    _id: number;    
    health: number;
    attack: number;
    defense: number;
    name: string;
    source: string;
    maxHealth: number;

    getHitPoint(): number {
        let criticalHitChance = this._generateRandomNumber(2);
        let power = this.attack * criticalHitChance;

        return power;
    }
    
    getBlockPower(): number {
        let criticalHitChance = this._generateRandomNumber(2);
        let power = this.defense * criticalHitChance;

        return power;
    }

    private _generateRandomNumber(maxValue: number) : number{
        return Math.round(Math.random() * maxValue) + 1;
    }
}