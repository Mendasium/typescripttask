export class Atribute {
    key: string;
    value: string;

    public constructor(key: string, value: string) {
        this.key = key;
        this.value = value;
    }
  }